# PA_2022

21/03/2022

Portail a mettre en place sur EKS.

# Schema cible

![](./schema_portail.png)

# Dockerisation

docker build -t portail ./projetannuel/Dockerfile

docker container run -d -it -p 9080:443 portail

Variable d'environnement : 

    - SQL_IP : IP/FQDN de la bdd SQL  

# Avancement

Partie Django : OK

Gitlab CI/CD : OK

Apache2 : OK

Partie BDD : créer dans le Cloud

